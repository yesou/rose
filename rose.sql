/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : rose

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-08-01 07:18:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for rose_banner
-- ----------------------------
DROP TABLE IF EXISTS `rose_banner`;
CREATE TABLE `rose_banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '名称，作为标识',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_banner
-- ----------------------------
INSERT INTO `rose_banner` VALUES ('1', 'ISD', '的地方vm', '0', '0');

-- ----------------------------
-- Table structure for rose_banner_item
-- ----------------------------
DROP TABLE IF EXISTS `rose_banner_item`;
CREATE TABLE `rose_banner_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `img_id` int(11) NOT NULL COMMENT '外键,关联image表',
  `key_word` varchar(100) NOT NULL COMMENT '执行关键字，根据不同的type含义不同',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '跳转类型，可能指向商品，可能指向专题',
  `delete_time` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL COMMENT '关联banner',
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='banner 子项表';

-- ----------------------------
-- Records of rose_banner_item
-- ----------------------------
INSERT INTO `rose_banner_item` VALUES ('1', '1', '省道', '1', '0', '1', '0');

-- ----------------------------
-- Table structure for rose_category
-- ----------------------------
DROP TABLE IF EXISTS `rose_category`;
CREATE TABLE `rose_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'key',
  `name` char(15) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `sort` int(11) NOT NULL DEFAULT '110' COMMENT '排序',
  `desc` text,
  `topic_img_id` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_category
-- ----------------------------
INSERT INTO `rose_category` VALUES ('1', '春', '0', '0', '0', '17', null, null, null);
INSERT INTO `rose_category` VALUES ('2', '夏', '0', '0', '0', '18', null, null, null);
INSERT INTO `rose_category` VALUES ('3', '秋', '0', '0', '0', '19', null, null, null);
INSERT INTO `rose_category` VALUES ('4', '冬', '0', '0', '0', '20', null, null, null);
INSERT INTO `rose_category` VALUES ('5', '梅', '0', '0', '0', '21', null, null, null);
INSERT INTO `rose_category` VALUES ('6', '兰', '0', '0', '0', '22', null, null, null);
INSERT INTO `rose_category` VALUES ('7', '竹', '0', '0', '0', '23', null, null, null);

-- ----------------------------
-- Table structure for rose_image
-- ----------------------------
DROP TABLE IF EXISTS `rose_image`;
CREATE TABLE `rose_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(120) DEFAULT NULL COMMENT '图路径',
  `from` tinyint(2) DEFAULT NULL COMMENT '标识url\r\n相对：本地server --  1\r\n/其他网络的路径、云端图床\r\n（青牛的云存储，阿里云oss）-- 2',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_image
-- ----------------------------
INSERT INTO `rose_image` VALUES ('1', '/banner-a09.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('2', '/Theme-02.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('3', '/Theme-03.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('4', '/Theme-04.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('5', '/Theme-05.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('6', '/Theme-06.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('7', '/Theme-07.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('8', '/product-a01.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('9', '/product-a02.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('10', '/product-a03.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('11', '/product-a04.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('12', '/product-a05png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('13', '/product-a06.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('14', '/product-a07.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('15', '/product-a08.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('16', '/product-a09.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('19', '/Cate-03.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('20', '/Cate-04.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('21', '/Cate-05.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('22', '/Cate-06.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('23', '/Cate-07.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('17', '/Cate-a01.png', '1', '0', '0', '0');
INSERT INTO `rose_image` VALUES ('18', '/Cate-02.png', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for rose_order
-- ----------------------------
DROP TABLE IF EXISTS `rose_order`;
CREATE TABLE `rose_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(50) NOT NULL COMMENT '名称，作为标识',
  `user_id` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `status` tinyint(2) DEFAULT '0' COMMENT '0-未支付|\r\n1-已支付|\r\n2-已发货|\r\n3-已支付，但是库存不足|',
  `snap_name` varchar(255) DEFAULT NULL,
  `snap_img` varchar(255) DEFAULT NULL,
  `snap_items` varchar(255) DEFAULT NULL,
  `snap_address` varchar(255) DEFAULT NULL,
  `prepay_id` int(11) DEFAULT NULL COMMENT 'WxPay',
  `total_count` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_order
-- ----------------------------
INSERT INTO `rose_order` VALUES ('1', 'ISD', '1', '0.11', '0', null, null, null, null, null, null, '0', '0', null);
INSERT INTO `rose_order` VALUES ('2', 'A60454216190237059339', '3', '109.08', '0', '梨花带雨...', 'http://rose.cn/images/product-a01.png', '[{\"id\":1,\"isStock\":true,\"count\":2,\"name\":\"\\u68a8\\u82b1\\u5e26\\u96e8\",\"thisPrice\":24.24},{\"id\":2,\"isStock\":true,\"count\":3,\"name\":\"\\u7ea2\\u5bcc\\u58eb\",\"thisPrice\":36.36},{\"id\":3,\"isStock\":true,\"count\":4,\"name\":\"\\u5927\\u7ea2\\u67a3\",\"thisPrice\":48.48}]', '{\"id\":5,\"name\":null,\"user_id\":3,\"description\":\"{\\\"name\\\":\\\"BBBBBBBBBBB\\\",\\\"mobile\\\":\\\"15567890987\\\",\\\"province\\\":\\\"\\\\u5e7f\\\\u4e1c\\\",\\\"city\\\":\\\"\\\\u6df1\\\\u5733\\\",\\\"county\\\":\\\"\\\\u7f57\\\\u6e56\\\",\\\"detail\\\":\\\"\\\\u4ec0\\\\u4e48\\\\u9b3c\\\"}\"}', null, '9', null, null, null);
INSERT INTO `rose_order` VALUES ('3', 'A60454282510583959339', '3', '109.08', '0', '梨花带雨...', 'http://rose.cn/images/product-a01.png', '[{\"id\":1,\"isStock\":true,\"count\":2,\"name\":\"\\u68a8\\u82b1\\u5e26\\u96e8\",\"thisPrice\":24.24},{\"id\":2,\"isStock\":true,\"count\":3,\"name\":\"\\u7ea2\\u5bcc\\u58eb\",\"thisPrice\":36.36},{\"id\":3,\"isStock\":true,\"count\":4,\"name\":\"\\u5927\\u7ea2\\u67a3\",\"thisPrice\":48.48}]', '{\"id\":5,\"name\":null,\"user_id\":3,\"description\":\"{\\\"name\\\":\\\"BBBBBBBBBBB\\\",\\\"mobile\\\":\\\"15567890987\\\",\\\"province\\\":\\\"\\\\u5e7f\\\\u4e1c\\\",\\\"city\\\":\\\"\\\\u6df1\\\\u5733\\\",\\\"county\\\":\\\"\\\\u7f57\\\\u6e56\\\",\\\"detail\\\":\\\"\\\\u4ec0\\\\u4e48\\\\u9b3c\\\"}\"}', null, '9', null, null, null);
INSERT INTO `rose_order` VALUES ('4', 'A60455166494198859339', '3', '109.08', '0', '梨花带雨...', 'http://rose.cn/images/product-a01.png', '[{\"id\":1,\"isStock\":true,\"count\":2,\"name\":\"\\u68a8\\u82b1\\u5e26\\u96e8\",\"thisPrice\":24.24},{\"id\":2,\"isStock\":true,\"count\":3,\"name\":\"\\u7ea2\\u5bcc\\u58eb\",\"thisPrice\":36.36},{\"id\":3,\"isStock\":true,\"count\":4,\"name\":\"\\u5927\\u7ea2\\u67a3\",\"thisPrice\":48.48}]', '{\"id\":5,\"name\":null,\"user_id\":3,\"description\":\"{\\\"name\\\":\\\"BBBBBBBBBBB\\\",\\\"mobile\\\":\\\"15567890987\\\",\\\"province\\\":\\\"\\\\u5e7f\\\\u4e1c\\\",\\\"city\\\":\\\"\\\\u6df1\\\\u5733\\\",\\\"county\\\":\\\"\\\\u7f57\\\\u6e56\\\",\\\"detail\\\":\\\"\\\\u4ec0\\\\u4e48\\\\u9b3c\\\"}\"}', null, '9', null, '1496555166', '1496555166');
INSERT INTO `rose_order` VALUES ('5', 'A60455352201438559339', '3', '109.08', '0', '梨花带雨...', 'http://rose.cn/images/product-a01.png', '[{\"id\":1,\"isStock\":true,\"count\":2,\"name\":\"\\u68a8\\u82b1\\u5e26\\u96e8\",\"thisPrice\":24.24},{\"id\":2,\"isStock\":true,\"count\":3,\"name\":\"\\u7ea2\\u5bcc\\u58eb\",\"thisPrice\":36.36},{\"id\":3,\"isStock\":true,\"count\":4,\"name\":\"\\u5927\\u7ea2\\u67a3\",\"thisPrice\":48.48}]', '{\"id\":5,\"name\":null,\"user_id\":3,\"description\":\"{\\\"name\\\":\\\"BBBBBBBBBBB\\\",\\\"mobile\\\":\\\"15567890987\\\",\\\"province\\\":\\\"\\\\u5e7f\\\\u4e1c\\\",\\\"city\\\":\\\"\\\\u6df1\\\\u5733\\\",\\\"county\\\":\\\"\\\\u7f57\\\\u6e56\\\",\\\"detail\\\":\\\"\\\\u4ec0\\\\u4e48\\\\u9b3c\\\"}\"}', null, '9', '1496555352', '1496555352', null);
INSERT INTO `rose_order` VALUES ('6', 'A60455395442776959339', '3', '109.08', '0', '梨花带雨...', 'http://rose.cn/images/product-a01.png', '[{\"id\":1,\"isStock\":true,\"count\":2,\"name\":\"\\u68a8\\u82b1\\u5e26\\u96e8\",\"thisPrice\":24.24},{\"id\":2,\"isStock\":true,\"count\":3,\"name\":\"\\u7ea2\\u5bcc\\u58eb\",\"thisPrice\":36.36},{\"id\":3,\"isStock\":true,\"count\":4,\"name\":\"\\u5927\\u7ea2\\u67a3\",\"thisPrice\":48.48}]', '{\"id\":5,\"name\":null,\"user_id\":3,\"description\":\"{\\\"name\\\":\\\"BBBBBBBBBBB\\\",\\\"mobile\\\":\\\"15567890987\\\",\\\"province\\\":\\\"\\\\u5e7f\\\\u4e1c\\\",\\\"city\\\":\\\"\\\\u6df1\\\\u5733\\\",\\\"county\\\":\\\"\\\\u7f57\\\\u6e56\\\",\\\"detail\\\":\\\"\\\\u4ec0\\\\u4e48\\\\u9b3c\\\"}\"}', null, '9', null, '1496555395', '1496555395');

-- ----------------------------
-- Table structure for rose_order_product
-- ----------------------------
DROP TABLE IF EXISTS `rose_order_product`;
CREATE TABLE `rose_order_product` (
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_order_product
-- ----------------------------
INSERT INTO `rose_order_product` VALUES ('1', '2', '1');
INSERT INTO `rose_order_product` VALUES ('2', '1', '2');
INSERT INTO `rose_order_product` VALUES ('2', '2', '3');
INSERT INTO `rose_order_product` VALUES ('2', '3', '4');
INSERT INTO `rose_order_product` VALUES ('3', '1', '2');
INSERT INTO `rose_order_product` VALUES ('3', '2', '3');
INSERT INTO `rose_order_product` VALUES ('3', '3', '4');
INSERT INTO `rose_order_product` VALUES ('4', '1', '2');
INSERT INTO `rose_order_product` VALUES ('4', '2', '3');
INSERT INTO `rose_order_product` VALUES ('4', '3', '4');
INSERT INTO `rose_order_product` VALUES ('5', '1', '2');
INSERT INTO `rose_order_product` VALUES ('5', '2', '3');
INSERT INTO `rose_order_product` VALUES ('5', '3', '4');
INSERT INTO `rose_order_product` VALUES ('6', '1', '2');
INSERT INTO `rose_order_product` VALUES ('6', '2', '3');
INSERT INTO `rose_order_product` VALUES ('6', '3', '4');

-- ----------------------------
-- Table structure for rose_product
-- ----------------------------
DROP TABLE IF EXISTS `rose_product`;
CREATE TABLE `rose_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_img_url` varchar(129) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_product
-- ----------------------------
INSERT INTO `rose_product` VALUES ('1', '梨花带雨', '12.12', '909', '3', '/product-a01.png', '8', ' ', '1', '0', '0', '0');
INSERT INTO `rose_product` VALUES ('2', '红富士', '12.12', '100', '3', '/product-a02.png', '9', ' ', '1', '1', '0', '0');
INSERT INTO `rose_product` VALUES ('3', '大红枣', '12.12', '909', '3', '/product-a03.png', '10', ' ', '1', '2', '0', '0');
INSERT INTO `rose_product` VALUES ('4', '西瓜', '12.12', '909', '3', '/product-a04.png', '11', ' ', '1', '3', '0', '0');
INSERT INTO `rose_product` VALUES ('5', '蜜桃', '12.12', '909', '3', '/product-a05png', '12', ' ', '1', '4', '0', '0');
INSERT INTO `rose_product` VALUES ('6', '香瓜', '12.12', '909', '3', '/product-a06.png', '13', ' ', '1', '5', '0', '0');
INSERT INTO `rose_product` VALUES ('7', '坚果', '12.12', '909', '4', '/product-a07.png', '14', ' ', '1', '6', '0', '0');
INSERT INTO `rose_product` VALUES ('8', '核桃', '12.12', '909', '4', '/product-a08.png', '15', ' ', '1', '7', '0', '0');
INSERT INTO `rose_product` VALUES ('9', '花生', '12.12', '909', '4', '/product-a09.png', '16', ' ', '1', '8', '0', '0');
INSERT INTO `rose_product` VALUES ('10', '水蜜桃', '12.12', '909', '4', '/product-a10.png', '17', ' ', '1', '8', '0', '0');

-- ----------------------------
-- Table structure for rose_product_image
-- ----------------------------
DROP TABLE IF EXISTS `rose_product_image`;
CREATE TABLE `rose_product_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `img_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_product_image
-- ----------------------------
INSERT INTO `rose_product_image` VALUES ('1', '8', '1', '品名', '10', null);
INSERT INTO `rose_product_image` VALUES ('2', '9', '2', '口味', '10', null);
INSERT INTO `rose_product_image` VALUES ('3', '10', '3', '产地', '10', null);
INSERT INTO `rose_product_image` VALUES ('4', '11', '4', '保质期', '10', null);
INSERT INTO `rose_product_image` VALUES ('5', '12', '5', '净含量', '10', null);
INSERT INTO `rose_product_image` VALUES ('6', '13', '6', '品质', '10', null);
INSERT INTO `rose_product_image` VALUES ('7', '14', '7', '原料', '10', null);
INSERT INTO `rose_product_image` VALUES ('8', '15', '8', '工艺', '10', null);

-- ----------------------------
-- Table structure for rose_product_property
-- ----------------------------
DROP TABLE IF EXISTS `rose_product_property`;
CREATE TABLE `rose_product_property` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(28) DEFAULT NULL,
  `detail` text,
  `product_id` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_product_property
-- ----------------------------
INSERT INTO `rose_product_property` VALUES ('1', '品名', '蜜桃', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('2', '口味', '清甜,香甜,', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('3', '产地', '而耳朵塞,篱笆嫩', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('4', '保质期', '90天', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('5', '净含量', '489g', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('6', '品质', '良品', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('7', '原料', '蜜桃,香蜜素', '10', null, null);
INSERT INTO `rose_product_property` VALUES ('8', '工艺', '冷加工', '10', null, null);

-- ----------------------------
-- Table structure for rose_theme
-- ----------------------------
DROP TABLE IF EXISTS `rose_theme`;
CREATE TABLE `rose_theme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` text,
  `topic_img_id` int(11) DEFAULT NULL,
  `head_img_id` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_theme
-- ----------------------------
INSERT INTO `rose_theme` VALUES ('1', ' 专题栏目一', '彩虹水果世界', '7', '4', '0', '0');
INSERT INTO `rose_theme` VALUES ('2', ' 专题栏目二', '新品推荐', '2', '5', '0', '0');
INSERT INTO `rose_theme` VALUES ('3', ' 专题栏目三', '干果大世界', '3', '6', '0', '0');

-- ----------------------------
-- Table structure for rose_theme_product
-- ----------------------------
DROP TABLE IF EXISTS `rose_theme_product`;
CREATE TABLE `rose_theme_product` (
  `theme_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_theme_product
-- ----------------------------
INSERT INTO `rose_theme_product` VALUES ('1', '1');
INSERT INTO `rose_theme_product` VALUES ('1', '2');
INSERT INTO `rose_theme_product` VALUES ('1', '5');
INSERT INTO `rose_theme_product` VALUES ('2', '3');
INSERT INTO `rose_theme_product` VALUES ('2', '5');
INSERT INTO `rose_theme_product` VALUES ('2', '4');
INSERT INTO `rose_theme_product` VALUES ('3', '7');
INSERT INTO `rose_theme_product` VALUES ('3', '8');

-- ----------------------------
-- Table structure for rose_user
-- ----------------------------
DROP TABLE IF EXISTS `rose_user`;
CREATE TABLE `rose_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` char(32) DEFAULT NULL,
  `nickname` varchar(23) DEFAULT NULL COMMENT '名称，作为标识',
  `extend` varchar(149) DEFAULT NULL COMMENT '扩展信息',
  `description` text COMMENT '描述',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_user
-- ----------------------------
INSERT INTO `rose_user` VALUES ('1', null, 'ISD', null, '的地方vm', '0', '0', null);
INSERT INTO `rose_user` VALUES ('3', 'oiGwM0Vh8L_jWcBHv8lhAhFNZ-Ks', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for rose_user_address
-- ----------------------------
DROP TABLE IF EXISTS `rose_user_address`;
CREATE TABLE `rose_user_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '名称，作为标识',
  `user_id` int(11) unsigned DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rose_user_address
-- ----------------------------
INSERT INTO `rose_user_address` VALUES ('5', null, '3', '{\"name\":\"BBBBBBBBBBB\",\"mobile\":\"15567890987\",\"province\":\"\\u5e7f\\u4e1c\",\"city\":\"\\u6df1\\u5733\",\"county\":\"\\u7f57\\u6e56\",\"detail\":\"\\u4ec0\\u4e48\\u9b3c\"}', null, null);
